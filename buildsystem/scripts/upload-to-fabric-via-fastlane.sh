#!/bin/sh

# upload latest build to Fabric for actual deployment
set -e

# check env vars
: ${CIRCLE_BRANCH:?"CIRCLE_BRANCH is unset"}
: ${CIRCLE_BUILD_NUM:?"CIRCLE_BUILD_NUM is unset"}

echo "Uploading artifacts to Fabric via Fastlane"

fastlane debug