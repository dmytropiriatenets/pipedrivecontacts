package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class DealBlockOrderEntity {

    @SerializedName(TYPE)
    val type: String? = null

    @SerializedName(VISIBLE)
    val isVisible: Boolean = false

    companion object {
        const val TYPE = "type"
        const val VISIBLE = "visible"
    }
}