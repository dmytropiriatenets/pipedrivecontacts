package com.contacts.pipedrive.data.provider

import com.pipedrive.contacts.domain.provider.AuthDataProvider

class TokenAuthDataProvider(val token: String) : AuthDataProvider