package com.contacts.pipedrive.data.entities.persons.response

import com.google.gson.annotations.SerializedName

class PictureIdEntity {

    @SerializedName(ITEM_TYPE)
    val itemType: String? = null

    @SerializedName(ITEM_ID)
    val itemId: Long = 0

    @SerializedName(ACTIVE_FLAG)
    val activeFlag: Boolean = false

    @SerializedName(ADD_TIME)
    val addTime: String? = null

    @SerializedName(UPDATE_TIME)
    val updateTime: String? = null

    @SerializedName(ADDED_BY_USER_ID)
    val addedByUserId: Long = 0

    @SerializedName(PICTURES)
    val pictureMap: Map<Int, String>? = null

    @SerializedName(VALUE)
    val value: Long = 0

    companion object {
        const val ITEM_TYPE = "item_type"
        const val ITEM_ID = "item_id"
        const val ACTIVE_FLAG = "active_flag"
        const val ADD_TIME = "add_time"
        const val UPDATE_TIME = "update_time"
        const val ADDED_BY_USER_ID = "added_by_user_id"
        const val PICTURES = "pictures"
        const val VALUE = "value"
    }
}
