package com.contacts.pipedrive.data.mapper

abstract class Mapper<V, K> {
    abstract fun transform(inputObject: V): K

    fun transformAll(inputCollection: Collection<V>, outputCollection: MutableCollection<K>): Collection<K> {
        for (inputElement in inputCollection) {
            outputCollection.add(transform(inputElement))
        }
        return outputCollection
    }
}