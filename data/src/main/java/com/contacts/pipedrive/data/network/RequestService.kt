package com.contacts.pipedrive.data.network

import com.contacts.pipedrive.data.entities.authorization.request.AuthorizationRequestBody
import com.contacts.pipedrive.data.entities.authorization.response.AuthorizationResponse
import com.contacts.pipedrive.data.entities.persons.response.PersonListResponse
import com.pipedrive.contacts.domain.storage.CredentialStorage
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface RequestService {

    companion object {
        const val GET_PERSON_LIST_ENDPOINT = "persons"
        const val GET_AUTHORIZATIONS_ENDPOINT = "authorizations"
    }

    @POST(GET_AUTHORIZATIONS_ENDPOINT)
    fun authorizationList(@Body authorizationRequestBody: AuthorizationRequestBody): Single<AuthorizationResponse>

    @GET(GET_PERSON_LIST_ENDPOINT)
    fun contactList(): Single<PersonListResponse>

    object Factory {
        private val BASE_URL = "https://api.pipedrive.com/v1/"

        fun create(credentialStorage: CredentialStorage): RequestService {
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor { chain ->
                        chain.proceed(chain
                                .request()
                                .addQueryParameter("api_token", credentialStorage.getToken()))
                    }
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            return retrofit.create(RequestService::class.java)
        }
    }
}

fun Request.addQueryParameter(queryParamName: String, token: String?): Request? {
    val httpUrl = this
            .url()
            .newBuilder()
            .addQueryParameter(queryParamName, token)
            .build()
    val request = this
            .newBuilder()
            .url(httpUrl)
            .build()
    return request
}