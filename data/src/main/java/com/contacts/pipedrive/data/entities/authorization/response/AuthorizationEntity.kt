package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class AuthorizationEntity {

    @SerializedName(USER_ID)
    val userId: Long = 0

    @SerializedName(COMPANY_ID)
    val companyId: Long = 0

    @SerializedName(API_TOKEN)
    val apiToken: String? = null

    @SerializedName(ADD_TIME)
    val addTime: String? = null

    @SerializedName(COMPANY)
    val company: CompanyEntity? = null

    companion object {
        const val USER_ID = "user_id"
        const val COMPANY_ID = "company_id"
        const val API_TOKEN = "api_token"
        const val ADD_TIME = "add_time"
        const val COMPANY = "company"
    }
}