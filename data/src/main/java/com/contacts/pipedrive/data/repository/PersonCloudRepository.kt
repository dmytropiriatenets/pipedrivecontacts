package com.contacts.pipedrive.data.repository

import com.contacts.pipedrive.data.database.AppDatabase
import com.contacts.pipedrive.data.database.mapper.DBPersonEntityMapper
import com.contacts.pipedrive.data.database.mapper.PersonMapper
import com.contacts.pipedrive.data.entities.mapper.PersonEntityMapper
import com.contacts.pipedrive.data.network.RequestService
import com.pipedrive.contacts.domain.model.Person
import com.pipedrive.contacts.domain.repository.PersonRepository
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PersonCloudRepository
@Inject
constructor(private val requestService: RequestService,
            private val appDatabase: AppDatabase,
            private val dpPersonEntityDBMapper: DBPersonEntityMapper,
            private val personMapper: PersonMapper,
            private val personEntityResponseMapper: PersonEntityMapper) : PersonRepository {

    override fun loadPersonList(): Observable<Collection<Person>> {
        val requestObservable = requestService.contactList()
                .toObservable()
                .filter { it.isSuccess }
                .map {
                    it.dataCollection?.let { return@map personEntityResponseMapper.transformAll(it, ArrayList()) }
                    ArrayList<Person>()
                }
                .doOnNext {
                    val personDao = appDatabase.personDao()
                    personDao.deleteAll()
                    appDatabase.personDao().insertAll(dpPersonEntityDBMapper.transformAll(it, ArrayList()))
                }
        val personDaoObservable = appDatabase.personDao().getAll()
                .toObservable()
                .map { personMapper.transformAll(it, ArrayList()) }
        return Observable.concat(personDaoObservable, requestObservable)
    }
}