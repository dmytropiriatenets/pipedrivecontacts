package com.contacts.pipedrive.data.database.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "person")
data class DBPersonEntity(@PrimaryKey
                          @ColumnInfo var id: Long,
                          @ColumnInfo var firstName: String?,
                          @ColumnInfo var lastName: String?,
                          @ColumnInfo var name: String?,
                          @ColumnInfo var profileImage: String?,
                          @ColumnInfo var phoneString: String?,
                          @ColumnInfo var emailString: String?
)