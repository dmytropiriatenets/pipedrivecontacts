package com.contacts.pipedrive.data.storage

import com.contacts.pipedrive.data.sharedpreferences.SharedPrefs
import com.pipedrive.contacts.domain.storage.CredentialStorage
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPrefCredentialStorage
@Inject constructor(val sharedPrefs: SharedPrefs) : CredentialStorage {

    override fun saveToken(token: String) {
        sharedPrefs.token = token;
    }

    override fun getToken(): String? {
        return sharedPrefs.token
    }

    override fun isAuthorized(): Boolean {
        return sharedPrefs.token != ""
    }
}