package com.contacts.pipedrive.data.entities.authorization.request

import com.google.gson.annotations.SerializedName

class AuthorizationRequestBody(@SerializedName(EMAIL) var email: String,
                               @SerializedName(PASSWORD) var password: String) {
    companion object {
        const val EMAIL = "email"
        const val PASSWORD = "password"
    }
}