package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class SettingsEntity {

    @SerializedName(SHOW_GETTING_STARTED_VIDEO)
    val showGettingStartedVideo: Boolean = false

    @SerializedName(LIST_LIMIT)
    val listLimit: Long = 0

    @SerializedName(BETA_APP)
    val betaApp: Boolean = false

    @SerializedName(FILE_UPLOAD_DESTINATION)
    val fileUploadDestination: String? = null

    @SerializedName(CALLTO_LINK_SYNTAX)
    val calltoLinkSyntax: String? = null

    @SerializedName(AUTOFILL_DEAL_EXPECTED_CLOSE_DATE)
    val autofillDealExpectedCloseDate: Boolean = false

    @SerializedName(PERSON_DUPLICATE_CONDITION)
    val personDuplicateCondition: String? = null

    @SerializedName(ORGANIZATION_DUPLICATE_CONDITION)
    val organizationDuplicateCondition: String? = null

    @SerializedName(ADD_FOLLOWERS_WHEN_IMPORTING)
    val addFollowersWhenImporting: Boolean = false

    @SerializedName(SEARCH_BACKEND)
    val searchBackend: String? = null

    @SerializedName(BILLING_MANAGED_BY_SALES)
    val billingManagedBySales: Boolean = false

    @SerializedName(MAX_DEAL_AGE_IN_AVERAGE_PROGRESS_CALCULATION)
    val maxDealAgeInAverageProgressCalculation: Long = 0

    @SerializedName(THIRD_PARTY_LINKS)
    val thirdPartyLinks: List<String>? = null

    @SerializedName(ELASTIC_WRITE_TARGET_DURING_MIGRATION)
    val elasticWriteTargetDuringMigration: String? = null

    @SerializedName(AUTO_CREATE_NEW_PERSONS_FROM_FORWARDER_EMAILS)
    val autoCreateNewPersonsFromForwarderEmails: Boolean = false

    @SerializedName(COMPANY_ADVANCED_DEBUG_LOGS)
    val companyAdvancedDebugLogs: Boolean = false

    @SerializedName(DEAL_BLOCK_ORDER)
    val dealBlockOrder: List<DealBlockOrderEntity>? = null

    @SerializedName(PERSON_BLOCK_ORDER)
    val personBlockOrder: List<PersonBlockOrderEntity>? = null

    @SerializedName(ORGANIZATION_BLOCK_ORDER)
    val organizationBlockOrder: List<OrganizationBlockOrderEntity>? = null

    @SerializedName(NYLAS_SYNC)
    val nylasSync: Boolean = false

    @SerializedName(ONBOARDING_COMPLETE)
    val onboardingComplete: Boolean = false

    companion object {
        const val SHOW_GETTING_STARTED_VIDEO = "show_getting_started_video"
        const val LIST_LIMIT = "list_limit"
        const val BETA_APP = "beta_app"
        const val FILE_UPLOAD_DESTINATION = "file_upload_destination"
        const val CALLTO_LINK_SYNTAX = "callto_link_syntax"
        const val AUTOFILL_DEAL_EXPECTED_CLOSE_DATE = "autofill_deal_expected_close_date"
        const val PERSON_DUPLICATE_CONDITION = "person_duplicate_condition"
        const val ORGANIZATION_DUPLICATE_CONDITION = "organization_duplicate_condition"
        const val ADD_FOLLOWERS_WHEN_IMPORTING = "add_followers_when_importing"
        const val SEARCH_BACKEND = "search_backend"
        const val BILLING_MANAGED_BY_SALES = "billing_managed_by_sales"
        const val MAX_DEAL_AGE_IN_AVERAGE_PROGRESS_CALCULATION = "max_deal_age_in_average_progress_calculation"
        const val THIRD_PARTY_LINKS = "third_party_links"
        const val ELASTIC_WRITE_TARGET_DURING_MIGRATION = "elastic_write_target_during_migration"
        const val AUTO_CREATE_NEW_PERSONS_FROM_FORWARDER_EMAILS = "auto_create_new_persons_from_forwarder_emails"
        const val COMPANY_ADVANCED_DEBUG_LOGS = "company_advanced_debug_logs"
        const val DEAL_BLOCK_ORDER = "deal_block_order"
        const val PERSON_BLOCK_ORDER = "person_block_order"
        const val ORGANIZATION_BLOCK_ORDER = "organization_block_order"
        const val NYLAS_SYNC = "nylas_sync"
        const val ONBOARDING_COMPLETE = "onboarding_complete"
    }
}