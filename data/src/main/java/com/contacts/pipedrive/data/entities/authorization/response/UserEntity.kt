package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class UserEntity {

    @SerializedName(PROFILE)
    val profile: ProfileEntity? = null

    @SerializedName(LOCALE)
    val locale: LocaleEntity? = null

    @SerializedName(TIMEZONE)
    val timezone: TimezoneEntity? = null

    companion object {
        const val PROFILE = "profile"
        const val LOCALE = "locale"
        const val TIMEZONE = "timezone"
    }
}