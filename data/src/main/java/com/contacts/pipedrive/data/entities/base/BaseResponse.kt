package com.contacts.pipedrive.data.entities.base

import com.google.gson.annotations.SerializedName

abstract class BaseResponse<T> {

    companion object {
        const val SUCCESS = "success"
        const val DATA = "data"
        const val ADDITIONAL_DATA = "additional_data"
    }

    @SerializedName(SUCCESS)
    val isSuccess: Boolean = false

    @SerializedName(DATA)
    val dataCollection: Collection<T>? = null
}
