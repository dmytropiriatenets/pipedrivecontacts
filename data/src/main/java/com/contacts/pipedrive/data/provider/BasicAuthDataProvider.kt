package com.contacts.pipedrive.data.provider

import com.pipedrive.contacts.domain.provider.AuthDataProvider

class BasicAuthDataProvider(val email: String,
                            val password: String) : AuthDataProvider