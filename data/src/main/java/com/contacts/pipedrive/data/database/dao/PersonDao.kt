package com.contacts.pipedrive.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.contacts.pipedrive.data.database.entities.DBPersonEntity
import io.reactivex.Single


@Dao
interface PersonDao {

    @Query("SELECT * FROM person")
    fun getAll(): Single<List<DBPersonEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(personEntityCollection: Collection<DBPersonEntity>)

    @Query("SELECT * FROM person WHERE id = :id")
    fun getById(id: Long): Single<DBPersonEntity>

    @Query("DELETE FROM person")
    fun deleteAll()
}
