package com.contacts.pipedrive.data.handler

import com.contacts.pipedrive.data.provider.TokenAuthDataProvider
import com.pipedrive.contacts.domain.handler.AuthorizationHandler
import com.pipedrive.contacts.domain.model.Authorization
import com.pipedrive.contacts.domain.storage.CredentialStorage
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Scheduler
import javax.inject.Inject

class TokenAuthorizationHandler
@Inject
constructor(private val credentialStorage: CredentialStorage,
            private val observeScheduler: Scheduler,
            private val subscribeScheduler: Scheduler) : AuthorizationHandler<TokenAuthDataProvider> {

    override fun authorize(authDataProvider: TokenAuthDataProvider, subscriber: Observer<Authorization>) {
        Observable.just(authDataProvider)
                .observeOn(observeScheduler)
                .subscribeOn(subscribeScheduler)
                .map { Authorization(it.token) }
                .doOnNext { credentialStorage.saveToken(it.token) }
                .subscribe({
                    subscriber.onNext(it)
                }, {
                    subscriber.onError(it)
                }, {
                    subscriber.onComplete()
                })
    }
}