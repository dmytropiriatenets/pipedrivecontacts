package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class AdditionalDataEntity(@SerializedName(USER) val user: UserEntity? = null,
                           @SerializedName(MULTIPLE_COMPANIES) val multipleCompanies: Boolean? = null,
                           @SerializedName(DEFAULT_COMPANY_ID) val defaultCompanyId: Long? = null) {
    companion object {
        const val USER = "user"
        const val MULTIPLE_COMPANIES = "multiple_companies"
        const val DEFAULT_COMPANY_ID = "default_company_id"
    }
}