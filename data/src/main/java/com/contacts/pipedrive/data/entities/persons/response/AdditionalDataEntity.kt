package com.contacts.pipedrive.data.entities.persons.response

import com.google.gson.annotations.SerializedName

class AdditionalDataEntity {

    companion object {
        const val PAGINATION = "pagination"
    }

    @SerializedName(PAGINATION)
    val pagination: PaginationEntity? = null
}
