package com.contacts.pipedrive.data.entities.persons.response

import com.google.gson.annotations.SerializedName

class PersonEntity {

    @SerializedName(ID)
    val id: Long = 0

    @SerializedName(COMPANY_ID)
    val companyId: Long = 0

    @SerializedName(OWNER_ID)
    val owner: Any? = null

    @SerializedName(ORG_ID)
    private val organizationId: Any? = null

    @SerializedName(NAME)
    val name: String? = null

    @SerializedName(FIRST_NAME)
    val firstName: String? = null

    @SerializedName(LAST_NAME)
    val lastName: String? = null

    @SerializedName(OPEN_DEALS_COUNT)
    val openDealsCount: Long = 0

    @SerializedName(RELATED_OPEN_DEALS_COUNT)
    val relatedOpenDealsCount: Long = 0

    @SerializedName(CLOSED_DEALS_COUNT)
    val closedDealsCount: Long = 0

    @SerializedName(RELATED_CLOSED_DEALS_COUNT)
    val relatedClosedDealsCount: Long = 0

    @SerializedName(PARTICIPANT_OPEN_DEALS_COUNT)
    val participantOpenDealsCount: Long = 0

    @SerializedName(PARTICIPANT_CLOSED_DEALS_COUNT)
    val participantClosedDealsCount: Long = 0

    @SerializedName(EMAIL_MESSAGES_COUNT)
    val emailMessagesCount: Long = 0

    @SerializedName(ACTIVITIES_COUNT)
    val activitiesCount: Long = 0

    @SerializedName(DONE_ACTIVITIES_COUNT)
    val doneActivitiesCount: Long = 0

    @SerializedName(UNDONE_ACTIVITIES_COUNT)
    val undoneActivitiesCount: Long = 0

    @SerializedName(REFERENCE_ACTIVITIES_COUNT)
    val referenceActivitiesCount: Long = 0

    @SerializedName(FILES_COUNT)
    val filesCount: Long = 0

    @SerializedName(NOTES_COUNT)
    val notesCount: Long = 0

    @SerializedName(FOLLOWERS_COUNT)
    val followersCount: Long = 0

    @SerializedName(WON_DEALS_COUNT)
    val wonDealsCount: Long = 0

    @SerializedName(RELATED_WON_DEALS_COUNT)
    val relatedWonDealsCount: Long = 0

    @SerializedName(LOST_DEALS_COUNT)
    val lostDealsCount: Long = 0

    @SerializedName(RELATED_LOST_DEALS_COUNT)
    val relatedLostDealsCount: Long = 0

    @SerializedName(ACTIVE_FLAG)
    val isActive: Boolean = false

    @SerializedName(PHONE)
    val phoneEntityList: List<PhoneEntity> = ArrayList()

    @SerializedName(EMAIL)
    val emailEntityList: List<EmailEntity> = ArrayList()

    @SerializedName(FIRST_CHAR)
    val firstChar: String? = null

    @SerializedName(UPDATE_TIME)
    val updateTime: String? = null

    @SerializedName(ADD_TIME)
    val addTime: String? = null

    @SerializedName(VISIBLE_TO)
    val visibleTo: String? = null

    @SerializedName(PICTURE_ID)
    val pictureId: PictureIdEntity? = null

    @SerializedName(NEXT_ACTIVITY_DATE)
    val nextActivityDate: Long? = null

    @SerializedName(NEXT_ACTIVITY_TIME)
    val nextActivityTime: Long? = null

    @SerializedName(NEXT_ACTIVITY_ID)
    val nextActivityId: Long? = null

    @SerializedName(LAST_ACTIVITY_ID)
    val lastActivityId: Long? = null

    @SerializedName(LAST_ACTIVITY_DATE)
    val lastActivityDate: Long? = null

    @SerializedName(LAST_INCOMING_MAIL_TIME)
    val lastIncomingMailTime: Long? = null

    @SerializedName(LAST_OUTGOING_MAIL_TIME)
    val lastOutgoingMailTime: Long? = null

    @SerializedName(ORG_NAME)
    val orgName: String? = null

    @SerializedName(OWNER_NAME)
    val ownerName: String? = null

    @SerializedName(CC_EMAIL)
    val ccEmail: String? = null

    companion object {
        const val ID = "id"
        const val COMPANY_ID = "company_id"
        const val OWNER_ID = "owner_id"
        const val ORG_ID = "org_id"
        const val NAME = "name"
        const val FIRST_NAME = "first_name"
        const val LAST_NAME = "last_name"
        const val OPEN_DEALS_COUNT = "open_deals_count"
        const val RELATED_OPEN_DEALS_COUNT = "related_open_deals_count"
        const val CLOSED_DEALS_COUNT = "closed_deals_count"
        const val RELATED_CLOSED_DEALS_COUNT = "related_closed_deals_count"
        const val PARTICIPANT_OPEN_DEALS_COUNT = "participant_open_deals_count"
        const val PARTICIPANT_CLOSED_DEALS_COUNT = "participant_closed_deals_count"
        const val EMAIL_MESSAGES_COUNT = "email_messages_count"
        const val ACTIVITIES_COUNT = "activities_count"
        const val DONE_ACTIVITIES_COUNT = "done_activities_count"
        const val UNDONE_ACTIVITIES_COUNT = "undone_activities_count"
        const val REFERENCE_ACTIVITIES_COUNT = "reference_activities_count"
        const val FILES_COUNT = "files_count"
        const val NOTES_COUNT = "notes_count"
        const val FOLLOWERS_COUNT = "followers_count"
        const val WON_DEALS_COUNT = "won_deals_count"
        const val RELATED_WON_DEALS_COUNT = "related_won_deals_count"
        const val LOST_DEALS_COUNT = "lost_deals_count"
        const val RELATED_LOST_DEALS_COUNT = "related_lost_deals_count"
        const val ACTIVE_FLAG = "active_flag"
        const val PHONE = "phone"
        const val EMAIL = "email"
        const val FIRST_CHAR = "first_char"
        const val UPDATE_TIME = "update_time"
        const val ADD_TIME = "add_time"
        const val VISIBLE_TO = "visible_to"
        const val PICTURE_ID = "picture_id"
        const val NEXT_ACTIVITY_DATE = "next_activity_date"
        const val NEXT_ACTIVITY_TIME = "next_activity_time"
        const val NEXT_ACTIVITY_ID = "next_activity_id"
        const val LAST_ACTIVITY_ID = "last_activity_id"
        const val LAST_ACTIVITY_DATE = "last_activity_date"
        const val LAST_INCOMING_MAIL_TIME = "last_incoming_mail_time"
        const val LAST_OUTGOING_MAIL_TIME = "last_outgoing_mail_time"
        const val ORG_NAME = "org_name"
        const val OWNER_NAME = "owner_name"
        const val CC_EMAIL = "cc_email"
    }
}
