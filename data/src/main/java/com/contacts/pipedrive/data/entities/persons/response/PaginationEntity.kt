package com.contacts.pipedrive.data.entities.persons.response

import com.google.gson.annotations.SerializedName

class PaginationEntity {

    companion object {
        const val START = "start"
        const val LIMIT = "limit"
        const val MORE_ITEMS_IN_COLLECTION = "more_items_in_collection"
    }

    @SerializedName(START)
    val start: Long? = null

    @SerializedName(LIMIT)
    val limit: Long? = null

    @SerializedName(MORE_ITEMS_IN_COLLECTION)
    private val moreItemsInCollection: Boolean = false

    fun hasMoreItemsInCollection(): Boolean {
        return moreItemsInCollection
    }
}
