package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class CompanyEntity {

    @SerializedName(INFO)
    val info: InfoEntity? = null

    @SerializedName(FEATURES)
    val features: List<String>? = null

    @SerializedName(SETTINGS)
    val settings: SettingsEntity? = null

    companion object {
        const val INFO = "info"
        const val FEATURES = "features"
        const val SETTINGS = "settings"
    }
}