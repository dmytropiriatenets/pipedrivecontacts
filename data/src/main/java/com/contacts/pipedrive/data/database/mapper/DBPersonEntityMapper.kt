package com.contacts.pipedrive.data.database.mapper

import com.contacts.pipedrive.data.database.entities.DBPersonEntity
import com.contacts.pipedrive.data.mapper.Mapper
import com.google.gson.Gson
import com.pipedrive.contacts.domain.model.Person
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DBPersonEntityMapper
@Inject constructor() : Mapper<Person, DBPersonEntity>() {

    override fun transform(person: Person) = person.toDBPersonEntity()
}

fun Person.toDBPersonEntity() =
        DBPersonEntity(id = this.id,
                firstName = this.firstName,
                lastName = this.lastName,
                name = this.name,
                profileImage = this.profileImage,
                emailString = this.emailList.let {
                    Gson().toJson(it)
                },
                phoneString = this.phoneList.let {
                    Gson().toJson(it)
                })