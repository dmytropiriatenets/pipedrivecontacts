package com.contacts.pipedrive.data.entities.mapper

import com.contacts.pipedrive.data.entities.persons.response.EmailEntity
import com.contacts.pipedrive.data.mapper.Mapper
import com.pipedrive.contacts.domain.model.Email
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EmailEntityMapper
@Inject constructor() : Mapper<EmailEntity, Email>() {

    override fun transform(emailEntity: EmailEntity) = emailEntity.toEmail()
}

fun EmailEntity.toEmail() = Email(
        mail = this.value.let {
            it?.let { return@let "" }
            return@let it
        } as String,
        primary = this.primary.let {
            it?.let { return@let false }
            return@let it
        } as Boolean)