package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class TimezoneEntity {

    @SerializedName(NAME)
    val name: String? = null

    @SerializedName(OFFSET)
    val offset: Long = 0

    companion object {
        const val NAME = "name"
        const val OFFSET = "offset"
    }
}