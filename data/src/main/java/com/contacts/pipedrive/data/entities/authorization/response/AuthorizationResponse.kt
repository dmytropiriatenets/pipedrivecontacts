package com.contacts.pipedrive.data.entities.authorization.response

import com.contacts.pipedrive.data.entities.base.BaseResponse
import com.google.gson.annotations.SerializedName

class AuthorizationResponse : BaseResponse<AuthorizationEntity>() {

    @SerializedName(ADDITIONAL_DATA)
    val additionalData: AdditionalDataEntity? = null

}