package com.contacts.pipedrive.data.entities.persons.response

import com.contacts.pipedrive.data.entities.base.BaseResponse
import com.google.gson.annotations.SerializedName

class PersonListResponse : BaseResponse<PersonEntity>() {

    @SerializedName(ADDITIONAL_DATA)
    val additionalData: AdditionalDataEntity? = null
}