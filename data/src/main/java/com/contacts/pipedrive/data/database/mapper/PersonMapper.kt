package com.contacts.pipedrive.data.database.mapper

import com.contacts.pipedrive.data.database.entities.DBPersonEntity
import com.contacts.pipedrive.data.mapper.Mapper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pipedrive.contacts.domain.model.Email
import com.pipedrive.contacts.domain.model.Person
import com.pipedrive.contacts.domain.model.Phone
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PersonMapper
@Inject constructor() : Mapper<DBPersonEntity, Person>() {

    override fun transform(personEntity: DBPersonEntity) = personEntity.toPerson()
}

fun DBPersonEntity.toPerson() =
        Person(id = this.id,
                firstName = this.firstName,
                lastName = this.lastName,
                name = this.name,
                profileImage = this.profileImage,
                emailList = this.emailString.let {
                    Gson().fromJson<Collection<Email>>(it, object : TypeToken<Collection<Email>>() {}.type)
                },
                phoneList = this.phoneString.let {
                    Gson().fromJson<Collection<Phone>>(it, object : TypeToken<Collection<Phone>>() {}.type)
                }
        )