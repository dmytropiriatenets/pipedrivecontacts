package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class ProfileEntity {

    @SerializedName(ID)
    val id: Long = 0

    @SerializedName(EMAIL)
    val email: String? = null

    @SerializedName(NAME)
    val name: String? = null

    @SerializedName(IS_ADMIN)
    val isAdmin: Boolean = false

    @SerializedName(DEFAULT_CURRENCY)
    val defaultCurrency: String? = null

    @SerializedName(ICON_URL)
    val iconUrl: Any? = null

    @SerializedName(ACTIVATED)
    val isActivated: Boolean = false

    companion object {
        const val ID = "id"
        const val EMAIL = "email"
        const val NAME = "name"
        const val IS_ADMIN = "is_admin"
        const val DEFAULT_CURRENCY = "default_currency"
        const val ICON_URL = "icon_url"
        const val ACTIVATED = "activated"
    }
}