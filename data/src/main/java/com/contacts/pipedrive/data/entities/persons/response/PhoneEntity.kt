package com.contacts.pipedrive.data.entities.persons.response

import com.google.gson.annotations.SerializedName

class PhoneEntity {

    companion object {
        const val VALUE = "value"
        const val PRIMARY = "primary"
    }

    @SerializedName(VALUE)
    val value: String? = null

    @SerializedName(PRIMARY)
    val primary: Boolean? = null
}
