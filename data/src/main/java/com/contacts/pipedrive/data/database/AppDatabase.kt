package com.contacts.pipedrive.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

import com.contacts.pipedrive.data.database.dao.PersonDao
import com.contacts.pipedrive.data.database.entities.DBPersonEntity

@Database(entities = arrayOf(DBPersonEntity::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun personDao(): PersonDao
}