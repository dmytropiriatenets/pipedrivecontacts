package com.contacts.pipedrive.data.entities.mapper

import com.contacts.pipedrive.data.entities.persons.response.PhoneEntity
import com.contacts.pipedrive.data.mapper.Mapper
import com.pipedrive.contacts.domain.model.Phone
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PhoneEntityMapper
@Inject constructor() : Mapper<PhoneEntity, Phone>() {

    override fun transform(emailEntity: PhoneEntity) = emailEntity.toEmail()
}

fun PhoneEntity.toEmail() = Phone(
        number = this.value.let {
            it?.let { return@let "" }
            return@let it
        } as String,
        primary = this.primary.let {
            it?.let { return@let false }
            return@let it
        } as Boolean)