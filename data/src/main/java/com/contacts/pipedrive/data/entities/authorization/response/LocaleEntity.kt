package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class LocaleEntity {

    @SerializedName(LANGUAGE)
    val language: String? = null

    @SerializedName(COUNTRY)
    val country: String? = null

    @SerializedName(USES_12_HOUR_CLOCK)
    val isUses12HourClock: Boolean = false

    companion object {
        const val LANGUAGE = "language"
        const val COUNTRY = "country"
        const val USES_12_HOUR_CLOCK = "uses_12_hour_clock"
    }
}