package com.contacts.pipedrive.data.entities.authorization.response

import com.google.gson.annotations.SerializedName

class InfoEntity {

    @SerializedName(ID)
    val id: Long = 0

    @SerializedName(NAME)
    val name: String? = null

    @SerializedName(CREATOR_COMPANY_ID)
    val creatorCompanyId: String? = null

    @SerializedName(PLAN_ID)
    val planId: Long? = null

    @SerializedName(IDENTIFIER)
    val identifier: String? = null

    @SerializedName(DOMAIN)
    val domain: String? = null

    @SerializedName(BILLING_CURRENCY)
    val billingCurrency: String? = null

    @SerializedName(ADD_TIME)
    val addTime: String? = null

    @SerializedName(STATUS)
    val status: String? = null

    @SerializedName(TRIAL_ENDS)
    val trialEnds: String? = null

    @SerializedName(CANCELLED_FLAG)
    val isCancelledFlag: Boolean = false

    @SerializedName(CANCEL_TIME)
    val cancelTime: String? = null

    @SerializedName(COUNTRY)
    val country: String? = null

    @SerializedName(PROMO_CODE)
    val promoCode: String? = null

    @SerializedName(USED_PROMO_CODE_KEY)
    val usedPromoCodeKey: String? = null

    @SerializedName(ACCOUNT_IS_OPEN)
    val isAccountIsOpen: Boolean = false

    @SerializedName(ACCOUNT_IS_NOT_PAYING)
    val isAccountIsNotPaying: Boolean = false

    companion object {
        const val ID = "id"
        const val NAME = "name"
        const val CREATOR_COMPANY_ID = "creator_company_id"
        const val PLAN_ID = "plan_id"
        const val IDENTIFIER = "identifier"
        const val DOMAIN = "domain"
        const val BILLING_CURRENCY = "billing_currency"
        const val ADD_TIME = "add_time"
        const val STATUS = "status"
        const val TRIAL_ENDS = "trial_ends"
        const val CANCELLED_FLAG = "cancelled_flag"
        const val CANCEL_TIME = "cancel_time"
        const val COUNTRY = "country"
        const val PROMO_CODE = "promo_code"
        const val USED_PROMO_CODE_KEY = "used_promo_code_key"
        const val ACCOUNT_IS_OPEN = "account_is_open"
        const val ACCOUNT_IS_NOT_PAYING = "account_is_not_paying"
    }
}