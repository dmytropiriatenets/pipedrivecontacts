package com.contacts.pipedrive.data.handler

import com.contacts.pipedrive.data.entities.authorization.request.AuthorizationRequestBody
import com.contacts.pipedrive.data.network.RequestService
import com.contacts.pipedrive.data.provider.BasicAuthDataProvider
import com.pipedrive.contacts.domain.handler.AuthorizationHandler
import com.pipedrive.contacts.domain.model.Authorization
import com.pipedrive.contacts.domain.storage.CredentialStorage
import io.reactivex.Observer
import io.reactivex.Scheduler
import javax.inject.Inject

class BasicAuthorizationHandler
@Inject
constructor(private val requestService: RequestService,
            private val credentialStorage: CredentialStorage,
            private val observeScheduler: Scheduler,
            private val subscribeScheduler: Scheduler) : AuthorizationHandler<BasicAuthDataProvider> {

    override fun authorize(authDataProvider: BasicAuthDataProvider, subscriber: Observer<Authorization>) {
        requestService
                .authorizationList(AuthorizationRequestBody.fromProvider(authDataProvider))
                .observeOn(observeScheduler)
                .subscribeOn(subscribeScheduler)
                .filter {
                    it.isSuccess && it.dataCollection != null && it.dataCollection.isNotEmpty()
                }
                .map { Authorization(it.dataCollection!!.first().apiToken!!) }
                .doOnSuccess { credentialStorage.saveToken(it.token) }
                .subscribe({
                    subscriber.onNext(it)
                }, {
                    subscriber.onError(it)
                }, {
                    subscriber.onComplete()
                })
    }
}


fun AuthorizationRequestBody.Companion.fromProvider(authDataProvider: BasicAuthDataProvider) =
        authDataProvider.let {
            return@let AuthorizationRequestBody(email = it.email, password = it.password)
        }