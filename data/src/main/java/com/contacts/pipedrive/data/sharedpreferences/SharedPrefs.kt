package com.contacts.pipedrive.data.sharedpreferences

import android.app.Application
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPrefs
@Inject constructor(application: Application) {

    val PREFS_FILENAME = "preferences"
    val TOKEN = "token"
    val prefs: SharedPreferences = application.getSharedPreferences(PREFS_FILENAME, 0);

    var token: String
        get() = prefs.getString(TOKEN, "")
        set(value) = prefs.edit().putString(TOKEN, value).apply()
}