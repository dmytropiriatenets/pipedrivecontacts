package com.contacts.pipedrive.data.entities.mapper

import com.contacts.pipedrive.data.entities.persons.response.EmailEntity
import com.contacts.pipedrive.data.entities.persons.response.PersonEntity
import com.contacts.pipedrive.data.entities.persons.response.PhoneEntity
import com.contacts.pipedrive.data.mapper.Mapper
import com.pipedrive.contacts.domain.model.Email
import com.pipedrive.contacts.domain.model.Person
import com.pipedrive.contacts.domain.model.Phone
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PersonEntityMapper
@Inject
constructor(val phoneEntityMapper: Mapper<PhoneEntity, Phone>,
            val emailEntityMapper: Mapper<EmailEntity, Email>) : Mapper<PersonEntity, Person>() {

    override fun transform(personEntity: PersonEntity) = personEntity.toPerson(phoneEntityMapper, emailEntityMapper)
}

fun PersonEntity.toPerson(phoneEntityMapper: Mapper<PhoneEntity, Phone>,
                          emailEntityMapper: Mapper<EmailEntity, Email>) =
        Person(id = this.id,
                firstName = this.firstName,
                lastName = this.lastName,
                name = this.name,
                profileImage = this.pictureId?.let {
                    it.pictureMap?.get(512) ?: it.pictureMap?.get(128)
                },
                phoneList = phoneEntityMapper.transformAll(this.phoneEntityList, ArrayList()),
                emailList = emailEntityMapper.transformAll(this.emailEntityList, ArrayList()))
