package com.pipedrive.contacts.domain.interactor

import com.pipedrive.contacts.domain.model.Person
import com.pipedrive.contacts.domain.repository.PersonRepository
import io.reactivex.Observable
import javax.inject.Inject

class PersonInteractor
@Inject
constructor(private val personRepository: PersonRepository) {
    fun loadPersonList(): Observable<Collection<Person>> = personRepository.loadPersonList()
}