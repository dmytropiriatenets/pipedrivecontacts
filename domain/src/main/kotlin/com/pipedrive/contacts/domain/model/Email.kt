package com.pipedrive.contacts.domain.model

data class Email(var mail: String,
                 var primary: Boolean)