package com.pipedrive.contacts.domain.model

data class Authorization(var token: String)