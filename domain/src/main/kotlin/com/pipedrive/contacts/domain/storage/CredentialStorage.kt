package com.pipedrive.contacts.domain.storage

interface CredentialStorage {
    fun saveToken(token: String)

    fun getToken(): String?

    fun isAuthorized(): Boolean
}