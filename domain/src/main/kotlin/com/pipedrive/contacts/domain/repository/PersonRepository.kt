package com.pipedrive.contacts.domain.repository

import com.pipedrive.contacts.domain.model.Person
import io.reactivex.Observable

interface PersonRepository {
    fun loadPersonList(): Observable<Collection<Person>>
}