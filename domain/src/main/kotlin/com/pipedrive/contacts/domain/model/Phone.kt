package com.pipedrive.contacts.domain.model

data class Phone(var number: String,
                 var primary: Boolean)