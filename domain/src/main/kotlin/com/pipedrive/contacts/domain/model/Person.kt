package com.pipedrive.contacts.domain.model

data class Person(var id: Long,
                  var name: String?,
                  var firstName: String?,
                  var lastName: String?,
                  var profileImage: String?,
                  var phoneList: Collection<Phone>,
                  var emailList: Collection<Email>)