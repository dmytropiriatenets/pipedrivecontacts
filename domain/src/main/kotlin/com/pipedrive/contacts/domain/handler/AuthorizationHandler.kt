package com.pipedrive.contacts.domain.handler

import com.pipedrive.contacts.domain.model.Authorization
import com.pipedrive.contacts.domain.provider.AuthDataProvider
import io.reactivex.Observer

interface AuthorizationHandler<in T : AuthDataProvider> {
    fun authorize(authDataProvider: T, subscriber: Observer<Authorization>)
}
