package com.pipedrive.contacts.presentation.ui.personlist;

import android.databinding.BindingAdapter;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;
import com.pipedrive.contacts.presentation.databinding.ItemPersonBinding;
import com.pipedrive.contacts.presentation.model.PersonItem;

public class PersonItemViewModel {

    private static final String TAG = PersonItemViewModel.class.getSimpleName();
    public PersonItem personItem;
    private ItemPersonBinding binding;
    private PersonListAdapter.PersonSelectedListener personSelectedListener;

    public void setPersonItem(PersonItem personItem) {
        this.personItem = personItem;
    }

    public void setBinding(ItemPersonBinding binding) {
        this.binding = binding;
    }

    @BindingAdapter({"imageUrl", "bindingModel"})
    public static void setCoverUrl(SimpleDraweeView imageView, String imageUrl, final PersonItemViewModel bindingModel) {
        if (TextUtils.isEmpty(imageUrl)) {
            return;
        }
        Uri imageUri = Uri.parse(imageUrl);
        imageView.setImageURI(imageUri);
    }

    @SuppressWarnings("unused")
    public void onItemSelected(View view) {
        personSelectedListener.onPersonSelected(personItem.getId());
    }

    public void setPersonSelectedListener(PersonListAdapter.PersonSelectedListener personSelectedListener) {
        this.personSelectedListener = personSelectedListener;
    }
}