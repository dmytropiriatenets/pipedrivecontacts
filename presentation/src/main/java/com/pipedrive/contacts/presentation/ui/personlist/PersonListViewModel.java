package com.pipedrive.contacts.presentation.ui.personlist;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.util.Log;
import android.widget.Toast;

import com.pipedrive.contacts.domain.interactor.PersonInteractor;
import com.pipedrive.contacts.domain.model.Person;
import com.pipedrive.contacts.presentation.PipedriveContactsApplication;
import com.pipedrive.contacts.presentation.R;
import com.pipedrive.contacts.presentation.databinding.ActivityContactListBinding;
import com.pipedrive.contacts.presentation.mapper.PersonItemMapper;
import com.pipedrive.contacts.presentation.model.PersonItem;
import com.pipedrive.contacts.presentation.ui.base.BaseViewModel;
import com.pipedrive.contacts.presentation.ui.persondetails.PersonDetailsActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PersonListViewModel extends BaseViewModel<ActivityContactListBinding> {

    private static final String TAG = PersonListViewModel.class.getSimpleName();
    private final Observable<Collection<Person>> personListObservable;

    @Inject
    public PersonInteractor personInteractor;

    private PersonListAdapter personListAdapter;

    public PersonListViewModel(Application application) {
        super(application);
        ((PipedriveContactsApplication) getApplication()).getApplicationComponent().inject(this);
        personListObservable = personInteractor.loadPersonList()
                .subscribeOn(Schedulers.computation())
                .cache()
                .observeOn(AndroidSchedulers.mainThread())
                .replay(1)
                .autoConnect();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onActivityCreated() {
        personListAdapter = new PersonListAdapter(getApplication(), personId -> {
            Activity activity = activityRef.get();
            if (activity != null){
                PersonDetailsActivity.start(activity, personId);
            }
        });
        viewDataBinding.contactRecyclerView.setAdapter(personListAdapter);
        viewDataBinding.contactRecyclerView.addItemDecoration(new VerticalDividerItemDecoration(getApplication(), R.drawable.item_divider));
        personListObservable
                .map(people -> new PersonItemMapper().transformAll(people, new ArrayList<>()))
                .subscribe(this::handlePersonListLoaded,
                        this::handlePersonListFailed);
    }

    private void handlePersonListFailed(Throwable throwable) {
        Log.v(TAG, "Loading person list failed: " + throwable.getLocalizedMessage(), throwable);
        Toast.makeText(getApplication(), R.string.loading_person_list_failed, Toast.LENGTH_SHORT).show();
    }

    private void handlePersonListLoaded(Collection<PersonItem> personItemCollection) {
        PersonListAdapter adapter = PersonListViewModel.this.personListAdapter;
        List<PersonItem> newList = new ArrayList<>(personItemCollection);
        List<PersonItem> oldList = adapter.getList();
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtilCallback(oldList, newList));
        adapter.clear();
        adapter.addAll(personItemCollection);
        diffResult.dispatchUpdatesTo(adapter);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        viewDataBinding.setContactListViewModel(null);
        viewDataBinding.executePendingBindings();
    }

    public static class Factory extends BaseViewModel.Factory<ActivityContactListBinding> {

        public Factory(@NonNull Application application) {
            super(application);
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new PersonListViewModel(application);
        }
    }
}
