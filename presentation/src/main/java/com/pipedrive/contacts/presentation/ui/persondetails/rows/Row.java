package com.pipedrive.contacts.presentation.ui.persondetails.rows;

public abstract class Row {
    public abstract RowType getRowType();
}