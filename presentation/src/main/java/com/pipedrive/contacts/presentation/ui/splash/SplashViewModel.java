package com.pipedrive.contacts.presentation.ui.splash;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.view.View;

import com.pipedrive.contacts.domain.storage.CredentialStorage;
import com.pipedrive.contacts.presentation.PipedriveContactsApplication;
import com.pipedrive.contacts.presentation.databinding.ActivitySplashBinding;
import com.pipedrive.contacts.presentation.ui.authorization.BasicAuthorizationActivity;
import com.pipedrive.contacts.presentation.ui.authorization.TokenAuthorizationActivity;
import com.pipedrive.contacts.presentation.ui.base.BaseViewModel;
import com.pipedrive.contacts.presentation.ui.personlist.PersonListActivity;

import javax.inject.Inject;

public class SplashViewModel extends BaseViewModel<ActivitySplashBinding> {

    @Inject
    public CredentialStorage credentialStorage;

    public SplashViewModel(Application application) {
        super(application);
        ((PipedriveContactsApplication) getApplication()).getApplicationComponent().inject(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onCreate() {
        if (credentialStorage.isAuthorized()) {
            Activity activity = activityRef.get();
            if (activity != null) {
                PersonListActivity.start(activity);
            }
        }
    }

    @SuppressWarnings("unused")
    public void onLoginUsingPasswordPressed(View view) {
        Activity activity = activityRef.get();
        if (activity != null) {
            BasicAuthorizationActivity.start(activity);
        }
    }

    @SuppressWarnings("unused")
    public void onLoginUsingTokenPressed(View view) {
        Activity activity = activityRef.get();
        if (activity != null) {
            TokenAuthorizationActivity.start(activity);
        }
    }

    public static class Factory extends BaseViewModel.Factory<ActivitySplashBinding> {

        public Factory(@NonNull Application application) {
            super(application);
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new SplashViewModel(application);
        }
    }
}
