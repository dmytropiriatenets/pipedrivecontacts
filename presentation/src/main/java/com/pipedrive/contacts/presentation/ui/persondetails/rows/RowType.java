package com.pipedrive.contacts.presentation.ui.persondetails.rows;

public enum RowType {
    EMAIL(0),
    PHONE(1);

    private int value;

    RowType(int value) {
        this.value = value;
    }

    public static RowType fromValue(int value) {
        switch (value) {
            case 0:
                return EMAIL;
            case 1:
                return PHONE;
            default:
                return EMAIL;
        }
    }

    public int getValue() {
        return value;
    }
}