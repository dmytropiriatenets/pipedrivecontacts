package com.pipedrive.contacts.presentation.mapper;

import com.contacts.pipedrive.data.mapper.Mapper;
import com.pipedrive.contacts.domain.model.Email;
import com.pipedrive.contacts.presentation.model.EmailItem;

public class EmailItemMapper extends Mapper<Email, EmailItem> {

    @Override
    public EmailItem transform(Email email) {
        EmailItem emailItem = new EmailItem();
        emailItem.setPrimary(email.getPrimary());
        emailItem.setValue(email.getMail());
        return emailItem;
    }
}
