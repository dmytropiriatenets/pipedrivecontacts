package com.pipedrive.contacts.presentation.ui.authorization;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.contacts.pipedrive.data.handler.BasicAuthorizationHandler;
import com.contacts.pipedrive.data.provider.BasicAuthDataProvider;
import com.pipedrive.contacts.domain.model.Authorization;
import com.pipedrive.contacts.presentation.PipedriveContactsApplication;
import com.pipedrive.contacts.presentation.R;
import com.pipedrive.contacts.presentation.databinding.ActivityBasicAuthorizationBinding;
import com.pipedrive.contacts.presentation.ui.base.BaseViewModel;
import com.pipedrive.contacts.presentation.ui.personlist.PersonListActivity;
import com.pipedrive.contacts.presentation.utils.StringUtils;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class BasicAuthorizationViewModel extends BaseViewModel<ActivityBasicAuthorizationBinding> {

    private static final String TAG = BasicAuthorizationViewModel.class.getSimpleName();

    @Inject
    public BasicAuthorizationHandler authorizationHandler;

    private Observer<Authorization> authorizationObserver;
    public final ObservableField<Boolean> loginButtonEnabledObservable;
    public final ObservableField<Boolean> progressBarVisibilityObservable;

    public BasicAuthorizationViewModel(@NonNull Application application) {
        super(application);
        this.loginButtonEnabledObservable = new ObservableField<>(false);
        this.progressBarVisibilityObservable = new ObservableField<>(false);
        ((PipedriveContactsApplication) getApplication()).getApplicationComponent().inject(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onCreate() {
        viewDataBinding.invalidateAll();
    }

    @SuppressWarnings("unused")
    public void handleAuthorize(View view) {
        authorize();
    }

    @SuppressWarnings("unused")
    public void afterTextChanged(Editable editable) {
        invalidateAuthorizeField();
    }

    @SuppressWarnings("unused")
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            String email = viewDataBinding.emailEditText.getText().toString().trim();
            String password = viewDataBinding.passwordEditText.getText().toString().trim();
            if (isLoginPasswordValid(email, password)) {
                authorize();
            }
        }
        return false;
    }

    private void authorize() {
        String email = viewDataBinding.emailEditText.getText().toString().trim();
        String password = viewDataBinding.passwordEditText.getText().toString().trim();
        BasicAuthDataProvider authDataProvider = new BasicAuthDataProvider(email, password);
        authorizationObserver = new Observer<Authorization>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Authorization authorization) {
                handleSuccessAuthorization(authorization);
            }

            @Override
            public void onError(Throwable e) {
                handleFailedAuthorization(e);
            }

            @Override
            public void onComplete() {
            }
        };
        authorizationHandler.authorize(authDataProvider, authorizationObserver);
        showProgressBar();
    }

    private void hideProgressBar() {
        progressBarVisibilityObservable.set(false);
    }

    private void showProgressBar() {
        progressBarVisibilityObservable.set(true);
    }

    private void invalidateAuthorizeField() {
        String email = viewDataBinding.emailEditText.getText().toString().trim();
        String password = viewDataBinding.passwordEditText.getText().toString().trim();
        loginButtonEnabledObservable.set(isLoginPasswordValid(email, password));
    }

    private boolean isLoginPasswordValid(String email, String password) {
        return StringUtils.isValidEmail(email) && !password.isEmpty();
    }

    private void handleFailedAuthorization(Throwable throwable) {
        Log.v(TAG, "authorization error: " + throwable.getLocalizedMessage(), throwable);
        hideProgressBar();
        Toast.makeText(getApplication(), R.string.login_failed, Toast.LENGTH_SHORT).show();
    }

    private void handleSuccessAuthorization(Authorization authorization) {
        hideProgressBar();
        Activity activity = activityRef.get();
        if (activity != null) {
            PersonListActivity.start(activity);
        }
    }

    public static class Factory extends BaseViewModel.Factory<ActivityBasicAuthorizationBinding> {

        public Factory(@NonNull Application application) {
            super(application);
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new BasicAuthorizationViewModel(application);
        }
    }
}
