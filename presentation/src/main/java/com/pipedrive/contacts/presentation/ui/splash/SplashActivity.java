package com.pipedrive.contacts.presentation.ui.splash;

import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.android.databinding.library.baseAdapters.BR;
import com.pipedrive.contacts.presentation.R;
import com.pipedrive.contacts.presentation.databinding.ActivitySplashBinding;
import com.pipedrive.contacts.presentation.ui.base.BaseBindingActivity;

public class SplashActivity extends BaseBindingActivity<ActivitySplashBinding, SplashViewModel> {

    @Override
    protected int getViewModelDataBindingVariableId() {
        return BR.splashViewModel;
    }

    @Override
    public ViewModelProvider.Factory getViewModelFactory() {
        return new SplashViewModel.Factory(getApplication());
    }

    @NonNull
    @Override
    public Class<SplashViewModel> getViewModelClass() {
        return SplashViewModel.class;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }
}
