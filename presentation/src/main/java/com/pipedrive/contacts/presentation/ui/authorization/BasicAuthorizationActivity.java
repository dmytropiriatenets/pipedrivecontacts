package com.pipedrive.contacts.presentation.ui.authorization;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.android.databinding.library.baseAdapters.BR;
import com.pipedrive.contacts.presentation.R;
import com.pipedrive.contacts.presentation.databinding.ActivityBasicAuthorizationBinding;
import com.pipedrive.contacts.presentation.ui.base.BaseBindingActivity;

public class BasicAuthorizationActivity extends BaseBindingActivity<ActivityBasicAuthorizationBinding, BasicAuthorizationViewModel> {

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, BasicAuthorizationActivity.class));
    }

    @Override
    protected int getViewModelDataBindingVariableId() {
        return BR.basicAuthViewModel;
    }

    @Override
    public ViewModelProvider.Factory getViewModelFactory() {
        return new BasicAuthorizationViewModel.Factory(getApplication());
    }

    @NonNull
    @Override
    public Class<BasicAuthorizationViewModel> getViewModelClass() {
        return BasicAuthorizationViewModel.class;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_basic_authorization;
    }
}
