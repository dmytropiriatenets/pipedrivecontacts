package com.pipedrive.contacts.presentation.dagger.modules;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.contacts.pipedrive.data.database.AppDatabase;
import com.contacts.pipedrive.data.network.RequestService;
import com.contacts.pipedrive.data.repository.PersonCloudRepository;
import com.contacts.pipedrive.data.sharedpreferences.SharedPrefs;
import com.contacts.pipedrive.data.storage.SharedPrefCredentialStorage;
import com.pipedrive.contacts.domain.interactor.PersonInteractor;
import com.pipedrive.contacts.domain.repository.PersonRepository;
import com.pipedrive.contacts.domain.storage.CredentialStorage;
import com.pipedrive.contacts.presentation.PipedriveContactsApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.annotations.NonNull;

@Module
public class ApplicationModule {

    private final PipedriveContactsApplication application;

    public ApplicationModule(PipedriveContactsApplication application) {
        this.application = application;
    }

    @Provides
    public Context provideAppContext() {
        return application;
    }

    @Provides
    @NonNull
    @Singleton
    public RequestService createNetworkService(CredentialStorage credentialStorage) {
        return RequestService.Factory.INSTANCE.create(credentialStorage);
    }

    @Provides
    @NonNull
    @Singleton
    PersonRepository providePersonRepository(PersonCloudRepository personCloudRepository) {
        return personCloudRepository;
    }

    @Provides
    @NonNull
    @Singleton
    PersonInteractor providePersonInteractor(PersonRepository personRepository) {
        return new PersonInteractor(personRepository);
    }

    @Provides
    @NonNull
    @Singleton
    CredentialStorage provideCredentialStorage(SharedPrefs sharedPrefs) {
        return new SharedPrefCredentialStorage(sharedPrefs);
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "pipedrive-db").build();
    }

    @Provides
    @Singleton
    SharedPrefs provideSharedPreferences() {
        return new SharedPrefs(application);
    }
}
