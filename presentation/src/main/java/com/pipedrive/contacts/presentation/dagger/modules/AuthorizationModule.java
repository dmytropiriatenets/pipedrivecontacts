package com.pipedrive.contacts.presentation.dagger.modules;

import com.contacts.pipedrive.data.handler.BasicAuthorizationHandler;
import com.contacts.pipedrive.data.handler.TokenAuthorizationHandler;
import com.contacts.pipedrive.data.network.RequestService;
import com.pipedrive.contacts.domain.storage.CredentialStorage;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public class AuthorizationModule {

    @Provides
    TokenAuthorizationHandler provideTokenAuthorizationHandler(CredentialStorage credentialStorage) {
        return new TokenAuthorizationHandler(credentialStorage, AndroidSchedulers.mainThread(), Schedulers.io());
    }

    @Provides
    BasicAuthorizationHandler provideBasicAuthorizationHandler(RequestService requestService, CredentialStorage credentialStorage) {
        return new BasicAuthorizationHandler(requestService, credentialStorage, AndroidSchedulers.mainThread(), Schedulers.io());
    }
}
