package com.pipedrive.contacts.presentation.ui.base;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class BaseBindingActivity<DB extends ViewDataBinding, VM extends BaseViewModel<DB>> extends BaseActivity {

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DB viewDataBinding = DataBindingUtil.setContentView(this, getLayoutResource());
        VM viewModel = ViewModelProviders.of(this, getViewModelFactory()).get(getViewModelClass());
        viewModel.attachViewBinding(this, viewDataBinding);
        viewDataBinding.setVariable(getViewModelDataBindingVariableId(), viewModel);
        getLifecycle().addObserver(viewModel);
    }

    protected abstract int getViewModelDataBindingVariableId();

    protected abstract ViewModelProvider.Factory getViewModelFactory();

    @NonNull
    protected abstract Class<VM> getViewModelClass();

    protected abstract int getLayoutResource();
}
