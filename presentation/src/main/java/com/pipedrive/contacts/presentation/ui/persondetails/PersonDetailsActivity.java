package com.pipedrive.contacts.presentation.ui.persondetails;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.android.databinding.library.baseAdapters.BR;
import com.pipedrive.contacts.presentation.R;
import com.pipedrive.contacts.presentation.databinding.ActivityContactDetailsBinding;
import com.pipedrive.contacts.presentation.ui.base.BaseBindingActivity;

public class PersonDetailsActivity extends BaseBindingActivity<ActivityContactDetailsBinding, PersonDetailsViewModel> {

    private static final String PERSON_ID = "personId";

    public static void start(Activity activity, long personId) {
        Intent intent = new Intent(activity, PersonDetailsActivity.class);
        intent.putExtra(PERSON_ID, personId);
        activity.startActivity(intent);
    }

    @Override
    protected int getViewModelDataBindingVariableId() {
        return BR.contactDetailsViewModel;
    }

    @Override
    public ViewModelProvider.Factory getViewModelFactory() {
        return new PersonDetailsViewModel.Factory(getApplication(), getIntent().getLongExtra(PERSON_ID, -1L));
    }

    @NonNull
    @Override
    public Class<PersonDetailsViewModel> getViewModelClass() {
        return PersonDetailsViewModel.class;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_contact_details;
    }
}
