package com.pipedrive.contacts.presentation.ui.persondetails.rows;

import com.pipedrive.contacts.presentation.model.EmailItem;

public class EmailRow extends Row {

    private EmailItem emailItem;

    public EmailRow(EmailItem emailItem) {
        this.emailItem = emailItem;
    }

    public EmailItem getEmailItem() {
        return emailItem;
    }

    @Override
    public RowType getRowType() {
        return RowType.EMAIL;
    }
}