package com.pipedrive.contacts.presentation.ui.persondetails;

import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.contacts.pipedrive.data.database.AppDatabase;
import com.contacts.pipedrive.data.database.entities.DBPersonEntity;
import com.contacts.pipedrive.data.database.mapper.PersonMapper;
import com.facebook.drawee.view.SimpleDraweeView;
import com.pipedrive.contacts.presentation.PipedriveContactsApplication;
import com.pipedrive.contacts.presentation.databinding.ActivityContactDetailsBinding;
import com.pipedrive.contacts.presentation.mapper.PersonItemMapper;
import com.pipedrive.contacts.presentation.model.EmailItem;
import com.pipedrive.contacts.presentation.model.PersonItem;
import com.pipedrive.contacts.presentation.model.PhoneItem;
import com.pipedrive.contacts.presentation.ui.base.BaseViewModel;
import com.pipedrive.contacts.presentation.ui.persondetails.rows.EmailRow;
import com.pipedrive.contacts.presentation.ui.persondetails.rows.PhoneRow;
import com.pipedrive.contacts.presentation.ui.persondetails.rows.Row;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PersonDetailsViewModel extends BaseViewModel<ActivityContactDetailsBinding> {

    private final Single<DBPersonEntity> personEntitySingle;
    @Inject
    public AppDatabase appDatabase;

    public PersonItem personItem;
    private PersonContactAdapter personContactAdapter;

    public PersonDetailsViewModel(Application application, long personId) {
        super(application);
        ((PipedriveContactsApplication) getApplication()).getApplicationComponent().inject(this);
        personEntitySingle = appDatabase.personDao().getById(personId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .cache();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onActivityCreated() {
        personContactAdapter = new PersonContactAdapter(getApplication());
        viewDataBinding.contactRecyclerView.setAdapter(personContactAdapter);
        Single<PersonItem> personItemSingle = personEntitySingle
                .map(dbPersonEntity -> new PersonMapper().transform(dbPersonEntity))
                .map(person -> new PersonItemMapper().transform(person));
        personItemSingle.subscribe(personItem -> {
            PersonDetailsViewModel.this.personItem = personItem;
            viewDataBinding.executePendingBindings();
            viewDataBinding.invalidateAll();
        });
        personItemSingle
                .map(personItem -> {
                    List<Row> rowList = new ArrayList<>();
                    for (EmailItem emailItem : personItem.getEmailList()) {
                        if (TextUtils.isEmpty(emailItem.getValue())) {
                            continue;
                        }
                        rowList.add(new EmailRow(emailItem));
                    }
                    for (PhoneItem phoneItem : personItem.getPhoneList()) {
                        if (TextUtils.isEmpty(phoneItem.getValue())) {
                            continue;
                        }
                        rowList.add(new PhoneRow(phoneItem));
                    }
                    return rowList;
                }).subscribe(rowList -> {
                    personContactAdapter.clear();
                    personContactAdapter.addAll(rowList);
                    personContactAdapter.notifyDataSetChanged();
                }
        );
    }

    @BindingAdapter({"imageUrl"})
    public static void setCoverUrl(SimpleDraweeView imageView, String imageUrl) {
        if (TextUtils.isEmpty(imageUrl)) {
            return;
        }
        Uri imageUri = Uri.parse(imageUrl);
        imageView.setImageURI(imageUri);
    }

    public static class Factory extends BaseViewModel.Factory<ActivityContactDetailsBinding> {

        private final long personId;

        public Factory(@NonNull Application application, long personId) {
            super(application);
            this.personId = personId;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new PersonDetailsViewModel(application, personId);
        }
    }
}
