package com.pipedrive.contacts.presentation.ui.persondetails.rows;

import com.pipedrive.contacts.presentation.model.PhoneItem;

public class PhoneRow extends Row {

    private PhoneItem phoneItem;

    public PhoneRow(PhoneItem phoneItem) {
        this.phoneItem = phoneItem;
    }

    public PhoneItem getPhoneItem() {
        return phoneItem;
    }

    @Override
    public RowType getRowType() {
        return RowType.PHONE;
    }
}