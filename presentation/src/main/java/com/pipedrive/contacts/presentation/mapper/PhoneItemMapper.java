package com.pipedrive.contacts.presentation.mapper;

import com.contacts.pipedrive.data.mapper.Mapper;
import com.pipedrive.contacts.domain.model.Phone;
import com.pipedrive.contacts.presentation.model.PhoneItem;

public class PhoneItemMapper extends Mapper<Phone, PhoneItem> {

    @Override
    public PhoneItem transform(Phone phone) {
        PhoneItem phoneItem = new PhoneItem();
        phoneItem.setPrimary(phone.getPrimary());
        phoneItem.setValue(phone.getNumber());
        return phoneItem;
    }
}
