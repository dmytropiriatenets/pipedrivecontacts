package com.pipedrive.contacts.presentation.ui.personlist;

import android.support.v7.util.DiffUtil;

import com.pipedrive.contacts.presentation.model.PersonItem;

import java.util.List;

public class DiffUtilCallback extends DiffUtil.Callback {

    private final List<PersonItem> oldList;
    private final List<PersonItem> newList;

    public DiffUtilCallback(List<PersonItem> oldList, List<PersonItem> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getId().longValue() == newList.get(newItemPosition).getId().longValue();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return true;
    }
}
