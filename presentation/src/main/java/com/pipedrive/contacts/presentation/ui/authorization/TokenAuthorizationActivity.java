package com.pipedrive.contacts.presentation.ui.authorization;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.android.databinding.library.baseAdapters.BR;
import com.pipedrive.contacts.presentation.R;
import com.pipedrive.contacts.presentation.databinding.ActivityTokenAuthorizationBinding;
import com.pipedrive.contacts.presentation.ui.base.BaseBindingActivity;

public class TokenAuthorizationActivity extends BaseBindingActivity<ActivityTokenAuthorizationBinding, TokenAuthorizationViewModel> {

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, TokenAuthorizationActivity.class));
    }

    @Override
    protected int getViewModelDataBindingVariableId() {
        return BR.tokenAuthViewModel;
    }

    @Override
    public ViewModelProvider.Factory getViewModelFactory() {
        return new TokenAuthorizationViewModel.Factory(getApplication());
    }

    @NonNull
    @Override
    public Class<TokenAuthorizationViewModel> getViewModelClass() {
        return TokenAuthorizationViewModel.class;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_token_authorization;
    }
}
