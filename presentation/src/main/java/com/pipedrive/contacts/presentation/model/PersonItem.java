package com.pipedrive.contacts.presentation.model;

import java.util.Collection;

public class PersonItem {

    private Long id;
    private String firstName;
    private String lastName;
    private String name;
    private String profileImage;
    private Collection<PhoneItem> phoneList;
    private Collection<EmailItem> emailList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Collection<PhoneItem> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(Collection<PhoneItem> phoneList) {
        this.phoneList = phoneList;
    }

    public Collection<EmailItem> getEmailList() {
        return emailList;
    }

    public void setEmailList(Collection<EmailItem> emailList) {
        this.emailList = emailList;
    }
}
