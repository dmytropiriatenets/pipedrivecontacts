package com.pipedrive.contacts.presentation.ui.base;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;

public abstract class BaseViewModel<DB extends ViewDataBinding> extends AndroidViewModel implements LifecycleObserver {

    protected DB viewDataBinding;
    protected WeakReference<Activity> activityRef;

    public BaseViewModel(Application application) {
        super(application);
    }

    public void attachViewBinding(BaseActivity activity, DB viewDataBinding) {
        this.activityRef = new WeakReference<>(activity);
        this.viewDataBinding = viewDataBinding;
    }

    public static abstract class Factory<DB extends ViewDataBinding> implements ViewModelProvider.Factory {

        @NonNull
        protected final Application application;

        public Factory(@NonNull Application application) {
            this.application = application;
        }
    }
}
