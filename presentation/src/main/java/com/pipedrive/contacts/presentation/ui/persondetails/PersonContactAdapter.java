package com.pipedrive.contacts.presentation.ui.persondetails;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pipedrive.contacts.presentation.R;
import com.pipedrive.contacts.presentation.databinding.ListItemEmailBinding;
import com.pipedrive.contacts.presentation.databinding.ListItemPhoneBinding;
import com.pipedrive.contacts.presentation.model.EmailItem;
import com.pipedrive.contacts.presentation.model.PhoneItem;
import com.pipedrive.contacts.presentation.ui.persondetails.rows.EmailRow;
import com.pipedrive.contacts.presentation.ui.persondetails.rows.PhoneRow;
import com.pipedrive.contacts.presentation.ui.persondetails.rows.Row;
import com.pipedrive.contacts.presentation.ui.persondetails.rows.RowType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PersonContactAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Row> rowList;
    private final LayoutInflater layoutInflater;

    public PersonContactAdapter(Context context) {
        this.rowList = new ArrayList<>();
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void addAll(Collection<Row> rowCollection) {
        this.rowList.addAll(rowCollection);
    }

    public void clear() {
        this.rowList.clear();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (RowType.fromValue(viewType)) {
            case EMAIL:
                return new EmailContactViewHolder(this.layoutInflater.inflate(R.layout.list_item_email, null));
            case PHONE:
                return new PhoneContactViewHolder(this.layoutInflater.inflate(R.layout.list_item_phone, null));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Row row = this.rowList.get(position);
        switch (row.getRowType()) {
            case EMAIL:
                EmailRow emailRow = (EmailRow) row;
                ((EmailContactViewHolder) holder).bind(emailRow.getEmailItem());
                break;
            case PHONE:
                PhoneRow phoneRow = (PhoneRow) row;
                ((PhoneContactViewHolder) holder).bind(phoneRow.getPhoneItem());
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        return rowList.get(position).getRowType().getValue();
    }

    @Override
    public int getItemCount() {
        return this.rowList.size();
    }

    static class EmailContactViewHolder extends RecyclerView.ViewHolder {

        private final ListItemEmailBinding binding;

        public EmailContactViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public void bind(EmailItem emailItem) {
            binding.setEmailItem(emailItem);
        }
    }

    static class PhoneContactViewHolder extends RecyclerView.ViewHolder {

        private final ListItemPhoneBinding binding;

        public PhoneContactViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public void bind(PhoneItem phoneItem) {
            binding.setPhoneItem(phoneItem);
        }
    }
}
