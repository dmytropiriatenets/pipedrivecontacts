package com.pipedrive.contacts.presentation.dagger.modules;

import com.contacts.pipedrive.data.database.entities.DBPersonEntity;
import com.contacts.pipedrive.data.database.mapper.DBPersonEntityMapper;
import com.contacts.pipedrive.data.database.mapper.PersonMapper;
import com.contacts.pipedrive.data.entities.mapper.EmailEntityMapper;
import com.contacts.pipedrive.data.entities.mapper.PersonEntityMapper;
import com.contacts.pipedrive.data.entities.mapper.PhoneEntityMapper;
import com.contacts.pipedrive.data.entities.persons.response.EmailEntity;
import com.contacts.pipedrive.data.entities.persons.response.PersonEntity;
import com.contacts.pipedrive.data.entities.persons.response.PhoneEntity;
import com.contacts.pipedrive.data.mapper.Mapper;
import com.pipedrive.contacts.domain.model.Email;
import com.pipedrive.contacts.domain.model.Person;
import com.pipedrive.contacts.domain.model.Phone;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.annotations.NonNull;

@Module
public class MapperModule {

    @Provides
    @NonNull
    @Singleton
    Mapper<EmailEntity, Email> provideEmailEntityMapper() {
        return new EmailEntityMapper();
    }

    @Provides
    @NonNull
    @Singleton
    Mapper<PhoneEntity, Phone> providePhoneEntityMapper() {
        return new PhoneEntityMapper();
    }

    @Provides
    @NonNull
    @Singleton
    Mapper<PersonEntity, Person> providePersonEntityMapper(Mapper<PhoneEntity, Phone> phoneEntityMapper,
                                                           Mapper<EmailEntity, Email> emailEntityMapper) {
        return new PersonEntityMapper(phoneEntityMapper, emailEntityMapper);
    }

    @Provides
    @NonNull
    @Singleton
    Mapper<Person, DBPersonEntity> provideDBPersonEntityMapper() {
        return new DBPersonEntityMapper();
    }

    @Provides
    @NonNull
    @Singleton
    Mapper<DBPersonEntity, Person> providePersonMapper() {
        return new PersonMapper();
    }

}
