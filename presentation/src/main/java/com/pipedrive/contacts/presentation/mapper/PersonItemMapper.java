package com.pipedrive.contacts.presentation.mapper;

import com.contacts.pipedrive.data.mapper.Mapper;
import com.pipedrive.contacts.domain.model.Person;
import com.pipedrive.contacts.presentation.model.PersonItem;

import java.util.ArrayList;

public class PersonItemMapper extends Mapper<Person, PersonItem> {

    private final PhoneItemMapper phoneItemMapper;
    private final EmailItemMapper emailItemMapper;

    public PersonItemMapper() {
        phoneItemMapper = new PhoneItemMapper();
        emailItemMapper = new EmailItemMapper();
    }

    @Override
    public PersonItem transform(Person person) {
        PersonItem personItem = new PersonItem();
        personItem.setId(person.getId());
        personItem.setFirstName(person.getFirstName());
        personItem.setLastName(person.getLastName());
        personItem.setName(person.getName());
        personItem.setProfileImage(person.getProfileImage());
        personItem.setEmailList(emailItemMapper.transformAll(person.getEmailList(), new ArrayList<>()));
        personItem.setPhoneList(phoneItemMapper.transformAll(person.getPhoneList(), new ArrayList<>()));
        return personItem;
    }
}