package com.pipedrive.contacts.presentation.ui.authorization;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.contacts.pipedrive.data.handler.TokenAuthorizationHandler;
import com.contacts.pipedrive.data.provider.TokenAuthDataProvider;
import com.pipedrive.contacts.domain.model.Authorization;
import com.pipedrive.contacts.presentation.PipedriveContactsApplication;
import com.pipedrive.contacts.presentation.databinding.ActivityTokenAuthorizationBinding;
import com.pipedrive.contacts.presentation.ui.base.BaseActivity;
import com.pipedrive.contacts.presentation.ui.base.BaseViewModel;
import com.pipedrive.contacts.presentation.ui.personlist.PersonListActivity;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class TokenAuthorizationViewModel extends BaseViewModel<ActivityTokenAuthorizationBinding> {

    private static final String TAG = TokenAuthorizationViewModel.class.getSimpleName();

    @Inject
    TokenAuthorizationHandler authorizationHandler;

    private Observer<Authorization> authorizationObserver;
    public final ObservableField<Boolean> loginButtonEnabledObservable;

    public TokenAuthorizationViewModel(Application application) {
        super(application);
        this.loginButtonEnabledObservable = new ObservableField<>(false);
        ((PipedriveContactsApplication) getApplication()).getApplicationComponent().inject(this);
    }

    @SuppressWarnings("unused")
    public void afterTextChanged(Editable editable) {
        invalidateAuthorizeField();
    }

    @SuppressWarnings("unused")
    public void handleAuthorize(View view) {
        authorize();
    }

    @SuppressWarnings("unused")
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            String token = viewDataBinding.tokenEditText.getText().toString().trim();
            if (isTokenValid(token)) {
                authorize();
            }
        }
        return false;
    }

    private void invalidateAuthorizeField() {
        String token = viewDataBinding.tokenEditText.getText().toString().trim();
        loginButtonEnabledObservable.set(isTokenValid(token));
    }

    private void authorize() {
        String token = viewDataBinding.tokenEditText.getText().toString().trim();
        TokenAuthDataProvider authDataProvider = new TokenAuthDataProvider(token);
        authorizationObserver = new Observer<Authorization>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Authorization authorization) {
                handleSuccessAuthorization(authorization);
            }

            @Override
            public void onError(Throwable e) {
                handleFailedAuthorization(e);
            }

            @Override
            public void onComplete() {
            }
        };
        authorizationHandler.authorize(authDataProvider, authorizationObserver);
    }

    private boolean isTokenValid(String token) {
        return !TextUtils.isEmpty(token);
    }

    private void handleFailedAuthorization(Throwable throwable) {
        Log.v(TAG, "authorization error: " + throwable.getLocalizedMessage(), throwable);
    }

    private void handleSuccessAuthorization(Authorization authorization) {
        Activity activity = activityRef.get();
        if (activity != null) {
            activity.finish();
            PersonListActivity.start(activity);
        }
    }

    public static class Factory extends BaseViewModel.Factory<ActivityTokenAuthorizationBinding> {

        public Factory(@NonNull Application application) {
            super(application);
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new TokenAuthorizationViewModel(application);
        }
    }
}
