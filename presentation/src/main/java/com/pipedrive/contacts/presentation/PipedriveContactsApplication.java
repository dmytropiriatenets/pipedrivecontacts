package com.pipedrive.contacts.presentation;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.util.ByteConstants;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.cache.MemoryCacheParams;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.pipedrive.contacts.presentation.dagger.components.ApplicationComponent;
import com.pipedrive.contacts.presentation.dagger.components.DaggerApplicationComponent;
import com.pipedrive.contacts.presentation.dagger.modules.ApplicationModule;
import com.pipedrive.contacts.presentation.dagger.modules.AuthorizationModule;

import io.fabric.sdk.android.Fabric;

public class PipedriveContactsApplication extends Application {

    private ApplicationComponent applicationComponent;

    private final static int MAX_HEAP_SIZE = (int) Runtime.getRuntime().maxMemory();
    private final static int MAX_MEMORY_CACHE_SIZE = MAX_HEAP_SIZE / 4;
    private final static long MAX_DISK_CACHE_SIZE = 40L * ByteConstants.MB;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .authorizationModule(new AuthorizationModule())
                .build();
        ImagePipelineConfig pipelineConfig = ImagePipelineConfig.newBuilder(this)
                .setBitmapMemoryCacheParamsSupplier(() -> new MemoryCacheParams(
                        MAX_MEMORY_CACHE_SIZE,
                        Integer.MAX_VALUE,
                        MAX_MEMORY_CACHE_SIZE,
                        Integer.MAX_VALUE,
                        Integer.MAX_VALUE))
                .setMainDiskCacheConfig(DiskCacheConfig.newBuilder(this)
                        .setBaseDirectoryPath(getFilesDir())
                        .setBaseDirectoryName("images")
                        .setMaxCacheSize(MAX_DISK_CACHE_SIZE)
                        .build())
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(this, pipelineConfig);

    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
