package com.pipedrive.contacts.presentation.dagger.components;

import com.pipedrive.contacts.presentation.dagger.modules.ApplicationModule;
import com.pipedrive.contacts.presentation.dagger.modules.AuthorizationModule;
import com.pipedrive.contacts.presentation.dagger.modules.MapperModule;
import com.pipedrive.contacts.presentation.ui.authorization.BasicAuthorizationActivity;
import com.pipedrive.contacts.presentation.ui.authorization.BasicAuthorizationViewModel;
import com.pipedrive.contacts.presentation.ui.authorization.TokenAuthorizationViewModel;
import com.pipedrive.contacts.presentation.ui.persondetails.PersonDetailsViewModel;
import com.pipedrive.contacts.presentation.ui.personlist.PersonListViewModel;
import com.pipedrive.contacts.presentation.ui.splash.SplashViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {ApplicationModule.class, AuthorizationModule.class, MapperModule.class})
@Singleton
public interface ApplicationComponent {
    void inject(PersonListViewModel activity);

    void inject(BasicAuthorizationActivity activity);

    void inject(TokenAuthorizationViewModel activity);

    void inject(BasicAuthorizationViewModel basicAuthorizationViewModel);

    void inject(PersonDetailsViewModel personDetailsViewModel);

    void inject(SplashViewModel splashViewModel);
}
