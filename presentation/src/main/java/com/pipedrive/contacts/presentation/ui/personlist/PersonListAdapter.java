package com.pipedrive.contacts.presentation.ui.personlist;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pipedrive.contacts.presentation.R;
import com.pipedrive.contacts.presentation.databinding.ItemPersonBinding;
import com.pipedrive.contacts.presentation.model.PersonItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PersonListAdapter extends RecyclerView.Adapter<PersonListAdapter.PersonViewHolder> {

    private final List<PersonItem> personItemList;
    private final LayoutInflater inflater;
    private PersonSelectedListener personSelectedListener;

    PersonListAdapter(Context context, PersonSelectedListener personSelectedListener) {
        this.personItemList = new ArrayList<>();
        this.inflater = LayoutInflater.from(context);
        this.personSelectedListener = personSelectedListener;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new PersonViewHolder(this.inflater.inflate(R.layout.item_person, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int position) {
        personViewHolder.bind(this.personItemList.get(position), personSelectedListener);
    }

    public void addAll(Collection<PersonItem> personItemCollection) {
        this.personItemList.addAll(personItemCollection);
    }

    public List<PersonItem> getList() {
        return Collections.unmodifiableList(this.personItemList);
    }

    @Override
    public int getItemCount() {
        return this.personItemList.size();
    }

    public void clear() {
        this.personItemList.clear();
    }

    static class PersonViewHolder extends RecyclerView.ViewHolder {

        private final ItemPersonBinding itemPersonBinding;

        public PersonViewHolder(View itemView) {
            super(itemView);
            this.itemPersonBinding = DataBindingUtil.bind(itemView);
        }

        public void bind(PersonItem personItem, PersonSelectedListener personSelectedListener) {
            PersonItemViewModel personItemViewModel = new PersonItemViewModel();
            personItemViewModel.setPersonItem(personItem);
            personItemViewModel.setBinding(this.itemPersonBinding);
            personItemViewModel.setPersonSelectedListener(personSelectedListener);
            this.itemPersonBinding.setPersonItemViewModel(personItemViewModel);
        }
    }

    interface PersonSelectedListener {
        void onPersonSelected(long personId);
    }
}
