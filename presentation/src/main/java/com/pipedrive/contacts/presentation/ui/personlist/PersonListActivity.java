package com.pipedrive.contacts.presentation.ui.personlist;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.android.databinding.library.baseAdapters.BR;
import com.pipedrive.contacts.presentation.R;
import com.pipedrive.contacts.presentation.databinding.ActivityContactListBinding;
import com.pipedrive.contacts.presentation.ui.base.BaseBindingActivity;

public class PersonListActivity extends BaseBindingActivity<ActivityContactListBinding, PersonListViewModel> {

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, PersonListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    @Override
    protected int getViewModelDataBindingVariableId() {
        return BR.contactListViewModel;
    }

    @Override
    public ViewModelProvider.Factory getViewModelFactory() {
        return new PersonListViewModel.Factory(getApplication());
    }

    @NonNull
    @Override
    public Class<PersonListViewModel> getViewModelClass() {
        return PersonListViewModel.class;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_contact_list;
    }
}
